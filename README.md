# CartExampleApp

## Requirements
  1. Node v8+ if not istalled then istall it from https://nodejs.org/en/download/ as per you OS
  2. Angular cli if not installed then run `npm install -g @angular/cli`

## Development server

  1. Navigate to project root directory and run `npm install` to install all dependencies
  2. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.
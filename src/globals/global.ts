export const GLOBALS = {
  PRODUCTS: [
    { name: 'Sunflower Melody', image: 'assets/images/sunflower-melody.jpg', price: 30 },
    { name: 'Spring Mix', image: 'assets/images/spring-mix.jpg', price: 35 },
    { name: 'Colorful Arrangement', image: 'assets/images/colorful-arrangement.jpg', price: 40 },
    { name: 'Tulip Season', image: 'assets/images/tulip-season.jpg', price: 50 }
  ]
}
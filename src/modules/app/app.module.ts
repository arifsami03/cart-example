import { NgModule } from '@angular/core';

import { _IMPORTS, _DECLARATION, _PROVIDERS } from "./components.barrel";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components';



@NgModule({
  imports: [_IMPORTS, AppRoutingModule],
  declarations: [_DECLARATION],
  providers: [_PROVIDERS],
  bootstrap: [AppComponent]
})
export class AppModule { }

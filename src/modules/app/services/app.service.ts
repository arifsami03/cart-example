import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ProductModel } from '../models';

@Injectable()
export class AppService {
  private productItemSource = new Subject<ProductModel>();
  public productItem$ = this.productItemSource.asObservable();

  public setProductItem(item: ProductModel) {
    this.productItemSource.next(item);
  }

}
export * from './app/app.component';
export * from './header/header.component';
export * from './footer/footer.component';
export * from './product-list/product-list.component';
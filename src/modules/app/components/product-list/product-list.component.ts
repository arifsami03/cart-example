import { Component, OnInit } from '@angular/core';

import { AppService } from "@modules/app/services";

import { ProductModel } from "@modules/app/models";
import { GLOBALS } from "src/globals";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  public products: ProductModel[] = GLOBALS.PRODUCTS;

  constructor(private appService: AppService) {

    // list the success event from header add cart functionality
    this.appService.productItem$.subscribe(product => {
      this.products.push(product)
      this.prepareProductList();
    });
  }

  ngOnInit() {
    this.prepareProductList();
  }

  private prepareProductList() {
    this.products.map(product => {
      product.quantity = 1;
      product.inStock = this.generateRandomNumbersArray();
    });

  }

  private generateRandomNumbersArray() {
    const randomNumber = Math.floor(Math.random() * 20) + 1; // generate a random number between 1-20;
    let randomNumbersArray = [];

    for (let i = 1; i <= randomNumber; i++) {
      randomNumbersArray.push(i);
    }
    return randomNumbersArray;

  }

}
import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, transferArrayItem } from '@angular/cdk/drag-drop';

import { AppService } from "@modules/app/services";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public cartData: any = { totalAmount: 0, totalProductQuantity: 0, products: [] };

  constructor(private appService: AppService) { }

  ngOnInit() { }
  public dropInCart(event: CdkDragDrop<any[]>) {
    transferArrayItem(event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex);

    let product = event.container.data[event.currentIndex]; // current dragged product

    this.cartData.totalProductQuantity += Number(product.quantity);
    this.cartData.totalAmount += Number(product.quantity * product.price);

    // emit the product to re-add in product list
    this.appService.setProductItem(product);
  }

}
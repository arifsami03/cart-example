export class ProductModel {
  public id?: number;
  public name: string;
  public image: string;
  public price: number;
  public quantity?: number;
  public inStock?: number[];
}
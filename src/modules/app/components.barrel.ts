import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from "@angular/forms";
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { AppComponent, HeaderComponent, FooterComponent, ProductListComponent } from './components';

import { AppService } from './services';

export const _IMPORTS = [
  BrowserModule,
  BrowserAnimationsModule,
  FormsModule,
  FlexLayoutModule,
  MatIconModule,
  DragDropModule
];

export const _DECLARATION = [
  AppComponent,
  HeaderComponent,
  FooterComponent,
  ProductListComponent
];

export const _PROVIDERS = [AppService];